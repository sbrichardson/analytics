name: 'gitlab_snowflake'
version: '1.0'

profile: 'gitlab-snowflake'

source-paths: ["models"]
test-paths: ["tests"]
data-paths: ["data"]
macro-paths: ["macros"]

target-path: "target"
clean-targets:
    - "target"
    - "dbt_modules"

quoting:
    identifier: false
    schema: false

archive:
    - source_schema: analytics
      target_schema: dbt_archive
      tables:
        - source_table: sfdc_account
          target_table: sfdc_account_archived
          updated_at: _last_dbt_run::timestamp_ntz
          unique_key: account_id

        - source_table: sfdc_opportunity
          target_table: sfdc_opportunity_archived
          updated_at: _last_dbt_run::timestamp_ntz
          unique_key: opportunity_id

        - source_table: sfdc_users
          target_table: sfdc_users_archived
          updated_at: _last_dbt_run::timestamp_ntz
          unique_key: user_id_18__c

on-run-start:
    - "{{resume_warehouse(var('resume_warehouse', false), var('warehouse_name'))}}"
    - "{{create_udfs()}}"

on-run-end:
    - "grant usage on schema {{ target.schema }} to role reporter"
    - "grant select on all tables in schema {{ target.schema }} to role reporter"
    - "grant select on all views in schema {{ target.schema }} to role reporter"
    - "grant usage on schema {{ target.schema }}_meta to role reporter"
    - "grant select on all tables in schema {{ target.schema }}_meta to role reporter"
    - "grant select on all views in schema {{ target.schema }}_meta to role reporter"
    - "{{suspend_warehouse(var('suspend_warehouse', false), var('warehouse_name'))}}"

models:
    vars: 
        database: 'raw'
        warehouse_name: 'transforming'

    logging:
      pre-hook: "{{ logging.log_model_start_event() }}"
      post-hook: 
        - "{{ logging.log_model_end_event() }}"
        - "grant select on {{this}} to role reporter"

    snowplow:
        post-hook: "grant select on {{this}} to role reporter"
        vars:
          'snowplow:use_fivetran_interface': false
          'snowplow:events': "{{ref('unnested_events')}}"
          'snowplow:context:web_page': "{{ref('web_page')}}"
          'snowplow:context:performance_timing': false
          'snowplow:context:useragent': false
          'snowplow:timezone': 'America/New_York'
          'snowplow:page_ping_frequency': 30
          'snowplow:app_ids': ['gitlab']
        base:
          materialized: view
          optional:
            enabled: false
        page_views:
          optional:
            enabled: false

    gitlab_snowflake:
      post-hook: "grant select on {{this}} to role reporter"
      enabled: true
      materialized: view

      date:
        base:
          enabled: true
          materialized: table

      gitter:
        transformed:
          enabled: true
          materialized: table

      sheetload:
        xf:
          enabled: true
          materialized: table

      netsuite:
        xf:
          enabled: true
          materialized: table

      pings:
        clean:
          enabled: true
          materialized: table

      pipe2spend:
        enabled: true
        materialized: table

      retention:
        enabled: true
        materialized: table

      sfdc:
        transformed:
          enabled: true
          materialized: table

      zuora:
        enabled: true
        materialized: table
