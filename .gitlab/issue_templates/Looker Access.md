
I would like to request Looker Access for me or someone else. 
(Please submit one ticket per person.)

Please make sure you are following the [Security New Access Request](https://gitlab.com/gitlab-com/access-requests/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) process.

#### Name

(Your Name)

#### Gitlab Email

(Your Gitlab Email)

#### Team

(Your Team at Gitlab)

#### What's one question you hope you can use data to answer? 